/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Trabajos;

/**
 *
 * @author marcelo
 */
/* este programa pide al usuario que ingrese la temperatura
en grados celsius y lo transforma en grados kelvin y fahrenheit
*/

import java.util.Scanner;

public class Ejercicio8 {
    public static void main(String[] args) { 
        Scanner temperatura = new Scanner(System.in);
        
        System.out.println("Ingrese la temperatura en Celsius");
        double Celsius = temperatura.nextDouble();
        
        double kelvin = 273.15 + Celsius;
        double fahrenheit = 1.8 * Celsius;
        
        System.out.println("La temperatura en Kelvin es: " + kelvin);
        System.out.println("La temperatura en grados Fahrenheit es: " + fahrenheit);
        
      
    }
}

 



