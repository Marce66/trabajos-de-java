/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Trabajos;

/**
 *
 * @author marcelo
 */
/* este programa resuelve el calculo de la suma,
resta, multipĺicacion y division de dos numeros
*/
import java.util.Scanner;

public class Ejercicio3 { 
 public static void main(String[] args)  {  
     Scanner scanner = new Scanner(System.in);
     System.out.println("ingrese el primer numero");
     float numero1 = scanner.nextFloat();
     
     System. out.println("ingrese el segundo numero");
     float numero2 = scanner.nextFloat();
     
     float suma = numero1 + numero2;
     float resta = numero1 - numero2;
     float multiplicacion = numero1 * numero2;
     float division = numero1 / numero2;
     
     System.out.println("la suma es: " + suma);
     System.out.println("la resta es: " + resta);
     System.out.println("la multiplicacion es: " + multiplicacion);
     System.out.println("la division es: " + division);
     
 }
}

