/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Trabajos;

/**
 *
 * @author marcelo
 */
/* este programa devuelve dos edades, lugo 
   intercambia las edades
*/

import java.util.Scanner;
public class Ejercicio7 {
 public static void main(String[] args) {   
 Scanner scanner = new Scanner(System.in);
   
    System.out.println("ingrese la primera edad");
    int edad1 = scanner.nextInt();
    System.out.println("edad1: " + edad1);

    System.out.println("ingrese la segunda edad");
    int edad2 = scanner.nextInt();
    System.out.println("edad2: " + edad2);
 
     int aux;
     aux = edad1;
     edad1 = edad2;
     edad2 = aux;
     
     System.out.println("Después del intercambio:");
     System.out.println("edad1 = " + edad1);
     System.out.println("edad2 = " + edad2);
 }
}  
