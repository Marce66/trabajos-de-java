/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Trabajos;

/**
 *
 * @author marcelo
 */
/* Este programa pide al usuario que ingrese el precio de un producto y el porcentaje
de descuento. A continuación muestra por pantalla el importe
descontado y el importe a pagar.
*/

import java.util.Scanner;
public class Ejercicio6 {
 public static void main(String[] args) {   
 Scanner scanner = new Scanner(System.in);
   
    System.out.println("ingrese el precio");
    float precio = scanner.nextFloat();
    System.out.println("ingrese el descuento");
    float descuento = scanner.nextFloat();
    
    float importe_descontado = precio * (descuento / 100);
    float importe_pagar  = precio - descuento;
    System.out.println("El importe descontado es de: " + importe_descontado);
    System.out.println("El importe a pagar es de: " + importe_pagar);
 }
}  
