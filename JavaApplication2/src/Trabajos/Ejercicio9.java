/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Trabajos;

/**
 *
 * @author marcelo
 */
/* este programa cambia los pesos a dolares, euros, reales y guaranies.
*/


import java.util.Scanner;
public class Ejercicio9 {
    public static void main(String[] args) { 
        Scanner convertidor = new Scanner(System.in);
        
        System.out.printf("Ingrese la cantidad de pesos");
        double pesos = convertidor.nextDouble();
        
        double dolar = pesos / 231.68;
        double euro = pesos / 250.69;
        double guaranies = pesos / 31.00;
        double real = pesos / 46.81;
         
        System.out.printf("Equivalente en dólares: %.2f%n", dolar);
        System.out.printf("Equivalente en euros: %.2f%n", euro);
        System.out.printf("Equivalente en guaranies: %.2f%n", guaranies);
        System.out.printf("Equivalente en reales: %.2f%n", real);
    }
}


