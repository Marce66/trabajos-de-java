/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Trabajos;

/**
 *
 * @author marcelo
 */
/* este programa muestra el resultado 
del area y perimetro del radio de una circunferencia
*/

import java.util.Scanner;
public class Ejercicio5 {
 public static void main(String[] args) {   
 Scanner scanner = new Scanner(System.in);
     System.out.println("ingrese la el valor del radio");
     double radio = scanner.nextDouble();
     
     double area = Math.PI * Math.pow(radio, 2);
     double perimetro = 2 * Math.PI * radio;
 
     System.out.println("el area de la circunferencia es " + area);
     System.out.println("el perimetro de la circunferencia es " + perimetro);
  
 }

}  